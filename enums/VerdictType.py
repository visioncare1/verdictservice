from enum import Enum


class VerdictType(Enum):
    HEAVY = 1600
    LIGHT = 300
    MIXED = 800


def get_verdict(char_size):
    if char_size <= VerdictType.LIGHT.value:
        return VerdictType.LIGHT
    elif char_size <= VerdictType.MIXED.value:
        return VerdictType.MIXED

    return VerdictType.HEAVY
