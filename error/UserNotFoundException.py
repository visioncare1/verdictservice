class UserNotFoundException(RuntimeError):

    def __init__(self, user_id):
        self.user_id = user_id

    def message(self):
        return f'User with id {self.user_id} not found!'

