import repository.VerdictRepository as VerdictRepository
import enums.VerdictType as VerdictType
import util.PytesseractUtil as PytesseractUtil
import util.FileUtil as FileUtil
from PIL import Image
import os

PytesseractUtil.setup_pytesseract()


def save_verdict(user_id: str, image):
    path = os.path.join('temp', 'image.png')
    FileUtil.save_file(path, image)
    img = Image.open(path)
    char_size: int = PytesseractUtil.get_image_size(img)
    FileUtil.delete_file(path)
    verdict = VerdictType.get_verdict(char_size)
    user = VerdictRepository.save(user_id, verdict)
    return user.verdicts[-1]


