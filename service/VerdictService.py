import repository.VerdictRepository as VerdictRepository


def get_verdicts(user_id: str):
    return VerdictRepository.get_user_verdicts(user_id)


def get_verdicts_by_date(user_id: str, date_of_verdict):
    return VerdictRepository.get_verdicts_of_user_by_date(user_id, date_of_verdict)


def remove_verdicts_by_date(user_id: str, date_of_verdict):
    VerdictRepository.remove_verdicts_of_user_by_date(user_id, date_of_verdict)


def remove_verdicts(user_id: str):
    VerdictRepository.remove_verdicts(user_id)
