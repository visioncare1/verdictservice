import datetime
from typing import Type

import mongoengine
from model.User import User
from model.Verdict import Verdict
from enums.VerdictType import VerdictType
from error.UserNotFoundException import UserNotFoundException


def save(user_id: str, verdict_type: VerdictType) -> User:
    # Find user if not found, create a new User
    user: User = User.objects(id=user_id).first()
    if not user:
        user = User()

    # Fill out the Verdict
    verdict = Verdict()
    verdict.verdict = verdict_type.name.lower()

    # Append verdict to User
    user.id = user_id
    user.verdicts.append(verdict)
    user.save()

    return user


def get_user_verdicts(user_id: str):
    user = User.objects(id=user_id).first()

    if not user:
        raise UserNotFoundException(user_id)

    return user


def get_verdicts_of_user_by_date(user_id: str, date_of_verdict):
    user = User.objects \
        .filter(id=user_id).first()

    if not user:
        raise UserNotFoundException(user_id)

    user.verdicts = filter(lambda d: d.verdict_date.date() == date_of_verdict, user.verdicts)

    return user


def remove_verdicts_of_user_by_date(user_id: str, date_of_verdict):
    user = User.objects \
        .filter(id=user_id).first()

    if not user:
        raise UserNotFoundException(user_id)

    user.verdicts = list(filter(lambda d: d.verdict_date.date() != date_of_verdict, user.verdicts))
    user.save()


def remove_verdicts(user_id: str):
    user = User.objects \
            .filter(id=user_id).first()

    if not user:
        raise UserNotFoundException(user_id)

    user.verdicts = []
    user.save()
