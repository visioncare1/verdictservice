import datetime

from fastapi import FastAPI, File, UploadFile, Response, HTTPException
from mongoengine import connect
from mongoengine import connection
from Misc.ProjectVariables import *
import service.ImageService as ImageService
import service.VerdictService as VerdictService
from error.UserNotFoundException import UserNotFoundException
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event("startup")
async def create_db_client():
    try:
        # Add authentication
        connect(db=DB_NAME, host=MONGODB_ADDR, port=MONGODB_PORT, username=DB_USERNAME, password=DB_PASSWORD)
    except connection.ConnectionFailure:
        print("error connecting to mongodb")


@app.on_event("shutdown")
async def shutdown_db_client():
    pass


@app.get("/test")
def test_connection():
    return Response(content="{\"status\": \"OK\"}", media_type="application/json")


@app.get("/verdicts/{user_id}")
def get_verdicts(user_id: str):
    try:
        response_data = VerdictService.get_verdicts(user_id).to_json()
    except UserNotFoundException as e:
        raise HTTPException(status_code=401, detail=e.message())

    return Response(content=response_data, media_type="application/json")


@app.get("/verdicts/{user_id}/{date_of_verdict}")
def get_verdicts_by_date(user_id: str, date_of_verdict: datetime.date):
    try:
        response_data = VerdictService.get_verdicts_by_date(user_id, date_of_verdict).to_json()
    except UserNotFoundException as e:
        raise HTTPException(status_code=401, detail=e.message())

    return Response(content=response_data, media_type="application/json")


@app.post("/images/{user_id}")
def save_image_verdict(user_id: str, image: UploadFile = File(...)):
    response_data = ImageService.save_verdict(user_id, image).to_json()
    return Response(content=response_data, media_type="application/json")


@app.put('/images/{user_id}')
def update_image_verdict(user_id: str):
    raise HTTPException(status_code=501, detail='Requested endpoint is not implemented')


@app.delete("/verdicts/{user_id}/{date_of_verdict}")
def remove_verdicts_by_date(user_id: str, date_of_verdict: datetime.date):
    try:
        VerdictService.remove_verdicts_by_date(user_id, date_of_verdict)
    except UserNotFoundException as e:
        raise HTTPException(status_code=401, detail=e.message())

    return "success"


@app.delete("/verdicts/{user_id}")
def remove_all_user_verdicts(user_id: str):
    try:
        VerdictService.remove_verdicts(user_id)
    except UserNotFoundException as e:
        raise HTTPException(status_code=401, detail=e.message())

    return "success"
