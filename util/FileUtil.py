import shutil
import os
import logging


def save_file(path, obj):

    # Creating directory if it doesn't exist already
    dirname = os.path.dirname(path)
    directory_path = os.path.join(os.path.curdir, dirname)
    try:
        os.makedirs(directory_path, exist_ok=True)
        logging.info(f'Directory {directory_path} created successfully')
    except OSError as error:
        logging.info(f'Directory {directory_path} can not be created')

    # Saving the file
    with open(path, "wb") as buffer:
        shutil.copyfileobj(obj.file, buffer)
        logging.info(f'File {path} is created successfully')


def delete_file(path):
    try:
        os.remove(path)
        logging.info(f'File {path} removed successfully ')
    except OSError as error:
        logging.info(f'File {path} can not be removed')