import pytesseract
from Misc.ProjectVariables import *
from PIL import Image
import logging


def setup_pytesseract():
    pytesseract.pytesseract.tesseract_cmd = PYTESSERACT_PATH
    logging.info(f'Pytesseract is found by this path {PYTESSERACT_PATH}')


def get_image_size(img: Image):
    image_strep: str = pytesseract.image_to_string(img)
    image_strep = image_strep.replace(' ', '').replace('\n', '')
    return len(image_strep)

