import datetime

from mongoengine import StringField, IntField, DateTimeField
from mongoengine import EmbeddedDocument


class Verdict(EmbeddedDocument):
    verdict_date = DateTimeField(default=datetime.datetime.now)
    verdict = StringField(required=True)

