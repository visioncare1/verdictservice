from mongoengine import StringField, EmbeddedDocumentListField
from mongoengine import Document
from model.Verdict import Verdict


class User(Document):
    id = StringField(required=True, primary_key=True)
    verdicts = EmbeddedDocumentListField(Verdict)

    meta = {
        'collection': 'users'
    }
